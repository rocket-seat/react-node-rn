import React, { Component } from 'react';
import axios from 'axios'

import { View, Text, Button, TextInput } from 'react-native'

class App extends Component {

  state = {
    newTodoText: '',
    todos: []
  }

  async componentDidMount() {
    const resp = await axios.get("http://localhost:3333/todos")
    .then((response) => {
      console.log(response);
    }).catch((error) => {
      console.log(error);
    });

    this.setState({ todos: resp.data })
  }

  handleNewTodo = async (e) => {
    e.preventDefault();

    if (!this.state.newTodoText) return;

    const resp = await axios.post("http://localhost:3333/todos", {
      text: this.state.newTodoText
    }).then((response) => {
      console.log(response);
    }).catch((error) => {
      console.log(error);
    });

    this.setState({ todos: [...this.state.todos, resp.data] })
  }

  render() {
    return (
      <View style={{padding: 40}}>
        <TextInput onChangeText={text => this.setState({ newTodoText: text })} value={this.state.newTodoText} style={{height: 36, borderWidth: 1, borderColor: '#CCC'}} />
        <Button type='submit' onPress={this.handleNewTodo} title="Adicionar" />

        <View style={{marginTop: 20}}>
          {this.state.todos.map(obj => (
            <Text key={obj._id}>{obj.text}</Text>
          ))}
        </View>

      </View>
    );
  }
}

export default App;