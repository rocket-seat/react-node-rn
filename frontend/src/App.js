import React, { Component } from 'react';
import axios from 'axios'

class App extends Component {

  state = {
    newTodoText: '',
    todos: []
  }

  async componentDidMount() {
    const resp = await axios.get("http://localhost:3333/todos")
    this.setState({ todos: resp.data })
  }

  handleNewTodo = async (e) => {
    e.preventDefault();

    if(!this.state.newTodoText) return;

    const resp = await axios.post("http://localhost:3333/todos", {
      text: this.state.newTodoText
    })

    this.setState({todos: [...this.state.todos, resp.data]})
  }

  render() {
    return (
      <div className="App">

        <form onSubmit={this.handleNewTodo}>
          <input onChange={e => this.setState({ newTodoText: e.target.value })} value={this.state.newTodoText} />
          <button type='submit'>Adicionar</button>
        </form>

        <ul>
          {this.state.todos.map(obj => (
            <li key={obj._id}>{obj.text}</li>
          ))}
        </ul>

      </div>
    );
  }
}

export default App;
