const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const app = express();

app.use(express.json())
// app.use(cors())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

mongoose.connect('mongodb://localhost:27017/nodernreact', { useNewUrlParser: true })
require('./todo')

const Todo = mongoose.model('Todo')

app.get('/todos', async (req, res) => {
    const todos = await Todo.find()
    return res.json(todos)
})

app.post('/todos', async (req, res) => {
    const todo = await Todo.create(req.body);
    return res.json(todo)
})

app.listen(3333)