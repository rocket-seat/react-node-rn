const mongoose = require('mongoose')

const TodoSchema = new mongoose.Schema({
    text: {
        type: String,
        require: true
    }
})

mongoose.model('Todo', TodoSchema)